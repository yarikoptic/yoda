#! /usr/bin/env python

import yoda, random

p1 = yoda.Profile1D(20, 0.0, 100.0, path="/foo", title="MyTitle")

linspace = yoda.linspace(20, 0.0, 100.0)
p2 = yoda.Profile1D(linspace, path="/bar", title="Linearly spaced histo")

logspace = yoda.logspace(20, 1.0, 64)
p3 = yoda.Profile1D(logspace, path="/baz", title="Log-spaced histo")


NUM_SAMPLES = 1000
for i in range(NUM_SAMPLES):
    # exp = - (i-NUM_SAMPLES/2)**2 / float(NUM_SAMPLES/4)
    # val = 2.718 ** exp
    val = random.uniform(0,100)
    p1.fill(val, random.gauss(5, 3));
    p2.fill(val, random.gauss(5, 4));
    p3.fill(val, random.gauss(5, 5));
print(p1)
print(p2)
print(p3)


yoda.write([p1,p2,p3], "p1d.yoda")
aos = yoda.read("p1d.yoda")
for _, ao in aos.items():
    print(ao)

yoda.writeFLAT([p1,p2,p3], "p1d.dat")
aos = yoda.read("p1d.dat")
for _, ao in aos.items():
    print(ao)


# Check that the bin scaling is done properly
s1 = p1.mkScatter()
if p1.numBins() != s1.numPoints():
    print("FAIL mkScatter() #bin={} -> #point={}".format(p1.numBins(), s1.numPoints()))
    exit(11)
yVal = p1.bin(1).yMean()
if yVal != s1.point(0).y():
    print("FAIL mkScatter() bin0 value={} -> bin0 value={}".format(yVal, s1.point(0).y()))
    exit(12)
yErr = p1.bin(1).yStdErr()
if yErr != s1.point(0).yErrAvg():
    print("FAIL mkScatter() bin0 err={} -> point0 err={}".format(yErr, s1.point(0).yErrAvg()))
    exit(13)


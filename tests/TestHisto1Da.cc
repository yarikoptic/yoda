#include "YODA/Histo.h"
#include "YODA/Utils/Formatting.h"
#include <cmath>
#include <iostream>

using namespace std;
using namespace YODA;


int main() {

  Histo1D h(20, 0.0, 1.0);
  for (size_t n = 0; n < 1000; ++n) {
    const double num = rand()/static_cast<double>(RAND_MAX);
    h.fill(num);
  }

  MSG("Path = " << h.path());
  MSG("Mean value = " << h.xMean() << " +- " << h.xStdErr());
  MSG("Total area = " << h.integral());

  const double maxHeight = h.maxDensity();
  for (int i = 0; i < 4; ++i) {
    if (i > 0) h.rebin<0>(2);
    MSG("Histo (rebinning #" << i << ", num bins = " << h.numBins() << ")");
    for (const auto& b : h.bins()) {
      const int numElements = static_cast<int>(round(20 * b.sumW()/maxHeight));
      MSG(string().insert(0, numElements, '=') << "  " << RED(b.sumW()));
    }
  }

  return EXIT_SUCCESS;
}


#! /usr/bin/env python

import yoda
import os

import numpy
numpy.seterr(all='raise')

TESTSRCDIR = os.environ.get("YODA_TESTS_SRC", ".")
def testsrcpath(fname):
    return os.path.join(TESTSRCDIR, fname)

aos_ref = yoda.read(testsrcpath("test1.yoda"))
aos_key = list(aos_ref.keys())[0]
aos_obj = aos_ref[aos_key]

plotContent = {
                'plot features' : {
                            'Title'     : 'Test', 
                            'LogY'      : 0,
                            'RatioPlot' : True,
                            'XLabel'    : '$x$',
                            'YLabel'    : '$y$'
                            },
                'style' : 'default',
                'histograms' : {'Data'  : {'nominal' : aos_obj, 'ErrorBars' : True, 'Title' : 'Data' , 'IsRef' : True},
                                aos_key : {'nominal' : aos_obj, 'ErrorBars' : True, 'Title' : 'testMC'}
                                }

}

from yoda.plotting import script_generator
script_generator.process(plotContent, 'foobar', outdir=TESTSRCDIR, formats=["PDF", "PNG"])
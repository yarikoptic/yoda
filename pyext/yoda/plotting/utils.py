import yoda
import numpy as np


def safeDiv(numer, denom):
    """Simple division method with implicit divByZero protection."""
    return np.divide(numer, denom,
                     out = np.where(numer != 0, np.nan, 1),
                     where = denom != 0)


def reshape(xprobe, xref, yvals, yerrs = None):
    """Helper method to reshape yvals and yerrs to match shape of reference curve.
       If the probe has fewer elements than the reference, pad using NaNs."""
    padded_yvals = np.array([ yvals[x==xprobe][0] if x in xprobe else \
                              np.nan for x in xref ])
    padded_yerrs = None if yerrs is None else \
                   np.array([ [errs[x==xprobe][0] if x in xprobe else \
                               np.nan for x in xref ] for errs in yerrs ])
    return padded_yvals, padded_yerrs


def mkPlotFriendlyScatter(ao):
  """Converts non-scatter AOs to scatters. If the AO has masked bins,
     the corresponding indices are stored in the metadata.If the AO has
     discrete binning, the edge labels are also stored in the metadata."""
  origT = str(type(ao))
  rtn = ao.mkScatter(ao.path()) if 'Binned' not in origT else \
        ao.mkScatter(ao.path(), includeOverflows=False,
                     includeMaskedBins=True) if 'Estimate' in origT else \
        ao.mkScatter(ao.path(), binwidthdiv=True, useFocus=False,
                     includeOverflows=False,includeMaskedBins=True)
  if hasattr(ao, 'binDim'):
      # if there are masked bins, find them and set value to NaN
      if len(ao.maskedBins()) > 0:
          maskIdx = -1
          for b in ao.bins(False, True):
              maskIdx += 1
              if b.isMasked():
                  rtn.point(maskIdx).setVal(ao.dim()-1, np.nan)

      # if there are discrete axes, set a +/-0.5 dummy uncertainty
      # and set custom tick-mark labels
      for i, axis in enumerate(ao.axisConfig().split(',')):
          if axis != 'd':
              # add dummy uncertainty
              for p in rtn.points():
                  p.setErr(i, 0.5)
              # decorate with custom labels
              if i < 3 and rtn.hasAnnotation('EdgesA%d' % (i+1)):
                  ALPHA = 'XYZ'
                  labels = rtn.annotation('EdgesA%d' % (i+1))
                  anno = '\t'.join([ '%d\t%s' % x for x in list(zip(rtn.vals(i),labels)) ])
                  rtn.setAnnotation(ALPHA[i]+'CustomMajorTicks', anno)
  return rtn



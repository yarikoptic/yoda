from __future__ import print_function

## Pull in core YODA C++/Python extension functionality
from yoda.core import *

__version__ = core.version()


## Pull in useful tools from submodules
from yoda.search import match_aos

## Try to pull in optional ROOT compatibility
try:
    import yoda.root
    HAS_ROOT_SUPPORT = True
except:
    HAS_ROOT_SUPPORT = False

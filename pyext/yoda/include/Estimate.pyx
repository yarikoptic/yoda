cimport util
cdef class Estimate(util.Base):
    """
    A point estimate, consisting of a central value and an error breakdown.
    """

    cdef c.Estimate* estptr(self) except NULL:
        return <c.Estimate *> self.ptr()

    def __init__(self):
        cutil.set_owned_ptr(self, new c.Estimate())


    def copy(self):
        return cutil.set_owned_ptr(self, new c.Estimate(deref(self.estptr())))


    def setVal(self, value):
       self.estptr().setVal(value)


    def setErr(self, *es):
       source = None
       es = list(es)
       if type(es[-1]) is str:
           source = es[-1]
           es = es[:-1]
       errs = es
       if source is None:
           source = ""
       #if isinstance(source, str):
       #    source = source.encode('utf-8')
       if len(errs) == 1:
           if not hasattr(errs[0], "__iter__"):
               self.estptr().setErr(util.read_symmetric(errs[0]), <string> source)
               return
           errs = errs[0]
       self.estptr().setErr(tuple(errs), <string> source)


    def set(self, val, errs, source = ""):
       self.setVal(val)
       self.setErr(errs, source)


    def reset(self):
        """
        () -> None

        Reset the estimates to the unfilled state.
        """
        self.estptr().reset()


    def val(self):
       return self.estptr().val()

    def errDownUp(self, source = ""):
       return tuple(self.estptr().errDownUp(source))

    def err(self, source = ""):
       return tuple(self.estptr().err(source))

    def errNegPos(self, source = ""):
       return tuple(self.estptr().errNegPos(source))

    def errDown(self, source = ""):
       return self.estptr().errDown(source)

    def errUp(self, source = ""):
       return self.estptr().errUp(source)

    def errNeg(self, source = ""):
       return self.estptr().errNeg(source)

    def errAvg(self, source = ""):
       return self.estptr().errAvg(source)

    def errPos(self, source = ""):
       return self.estptr().errPos(source)

    def relErrDownUp(self, source = ""):
       return tuple(self.estptr().relErrDownUp(source))

    def relErr(self, source = ""):
       return tuple(self.estptr().relErr(source))

    def relErrDown(self, source = ""):
       return self.estptr().relErrDown(source)

    def relErrUp(self, source = ""):
       return self.estptr().relErrUp(source)

    def quadSum(self):
       return tuple(self.estptr().quadSum())

    def quadSumNeg(self):
       return self.estptr().quadSumNeg()

    def quadSumPos(self):
       return self.estptr().quadSumPos()

    def totalErr(self):
       return tuple(self.estptr().totalErr())

    def relTotalErr(self):
       return tuple(self.estptr().relTotalErr())

    def relTotalErrNeg(self):
       return self.estptr().relTotalErrNeg()

    def relTotalErrPos(self):
       return self.estptr().relTotalErrPos()

    def relTotalErrAvg(self):
       return self.estptr().relTotalErrAvg()

    def sources(self):
       return self.estptr().sources()

    def serializeSources(self):
      return self.estptr().sources()

    def deserializeSources(self, data):
      cdef vector[string] cdata
      cdata = [ str(x) for x in data ]
      return self.estptr().deserializeSources(cdata)


    ## In-place special methods
    def __repr__(self):
        val = self.val()
        #lo,hi = self.quadSum()
        return '<Estimate(value=%.2e)>' % val

    def __add__(Estimate self, Estimate other):
        return cutil.new_owned_cls(Estimate, new c.Estimate(deref(self.estptr()) + deref(other.estptr())))

    def __sub__(Estimate self, Estimate other):
        return cutil.new_owned_cls(Estimate, new c.Estimate(deref(self.estptr()) - deref(other.estptr())))


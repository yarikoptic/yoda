// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_WRITERYODA_H
#define YODA_WRITERYODA_H

#include "YODA/Writer.h"

namespace YODA {


  /// Persistency writer for YODA flat text format.
  class WriterYODA : public Writer {
  public:

    /// Singleton creation function
    static Writer& create();

    // Include definitions of all write methods (all fulfilled by Writer::write(...))
    #include "YODA/WriterMethods.icc"


  protected:

    void writeAO(std::ostream& stream, const AnalysisObject& c);


  private:

    void _writeAnnotations(std::ostream& os, const AnalysisObject& ao);

    /// Private since it's a singleton.
    WriterYODA() { }

  };


}

#endif

// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#include <string>
#include <vector>

namespace YODA {


  /// Get the path to the directory containing libYODA
  std::string getLibPath();

  /// Get the path to the installed share/YODA/ data directory
  std::string getDataPath();

  /// YODA data paths
  std::vector<std::string> getYodaDataPath();


}

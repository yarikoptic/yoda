// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_Fillable_h
#define YODA_Fillable_h

namespace YODA {


  /// A base class for all fillable objects
  class Fillable {
  public:

    /// @name Constructors
    //@{

    /// Virtual destructor for inheritance
    virtual ~Fillable() = default;


    //@}

    /// @name Modifiers
    //@{

    /// Reset
    virtual void reset() = 0;

    /// Rescale as if all fill weights had been different by factor @a scalefactor
    virtual void scaleW(double scalefactor) = 0;

    //@}

    /// @name Whole histo data
    //@{

    /// Fill-dimension of this data object
    virtual size_t fillDim() const noexcept = 0;

    /// Get the number of fills
    virtual double numEntries(const bool includeoverflows=true) const = 0;

    /// Get the effective number of fills
    virtual double effNumEntries(const bool includeoverflows=true) const = 0;

    /// Get sum of weights in histo
    virtual double sumW(const bool includeoverflows=true) const = 0;

    /// Get sum of squared weights in histo
    virtual double sumW2(const bool includeoverflows=true) const = 0;

    //@}

  };


}

#endif

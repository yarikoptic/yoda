// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2024 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_Profile2D_h
#define YODA_Profile2D_h

#include "YODA/Profile.h"
#pragma message "Profile2D.h is deprecated. Please use Profile.h instead."

#endif


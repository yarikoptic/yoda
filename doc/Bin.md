# The Bin object

YODA's bin-wrapping class is templated on
* the binning dimension
* the content type
* the associated `Binning` type.

A simple example using `double` as the bin content type
using the `Binning` object defined [here](Binning.md):

```cpp
Bin<3, double, MyBinningT> bin1(85, binning);
std::cout << "My index is " << bin1.index();
std::cout << " and my differential volume is " << bin1.dVol();
std::cout << std::endl;
std::cout << "Along the continuous x-axis my (lower,upper) edges are ";
std::cout << "(" << bin1.xMin() << "," << bin1.xMax() << ")";
std::cout << std::endl;
```

```
My index is 85 and my differential volume is 2.2
Along the continuous x-axis my (lower,upper) edges are (0.1,2.3)
```

Note that some of the `min/max` methods are only enabled for continuous axes
because they are not defined on a discrete axis.
For instance, you will get a compiler error if you try to compute
the bin width along a discrete axis.

### Why is the bin dimension a template parameter?

The first template parameter seems redundant,
since the binning object knows abouts its dimensionality.
This is in fact a dummy parameter that allows us to mix-in
axis-specifc methods for the first few dimensions
(e.g. `xWidth()`, `yMax()`, `zMid()`, ...).
You could also use the `BinBase` class which
doesn't have this parameter, but then you could only ever
specify the respectice Axis index (0-indexed)
with a template parameter e.g. like so

```cpp
BinBase<double, MyBinningT> bin2(85, binning);
std::cout << "My index is " << bin2.index();
std::cout << " and my differential volume is " << bin2.dVol();
std::cout << std::endl;
std::cout << "Along the continuous x-axis my (lower,upper) edges are ";
std::cout << "(" << bin2.min<0>() << "," << bin2.max<0>() << ")";
std::cout << std::endl;
```

```
My index is 85 and my differential volume is 2.2
Along the continuous x-axis my (lower,upper) edges are (0.1,2.3)
```

The templated methods are also accessible through the nominal `Bin` class,
in addition to the more user-friendly axis-specific methods.

Note that bins don't have a standalone `width()` method,
since "bin width" is really an axis-dependent quantity:
it's the projection of the differential bin volume onto
a given axis, and so the dimension must be specified.
Convenient aliases exist to avoid the template parameter
exist for the first three dimensions, e.g.:

```cpp
assert(bin1.width<0>() == bin1.xWidth());
```


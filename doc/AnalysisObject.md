# AnalysisObject

YODA's `AnalysisObject` class is the base class
for all objects intended for direct use in analysis.
They provide the deriving object with a set of extendable
annotations, which include a standard set like
`type`, `path` and  `title`.

Standard YODA classes deriving from `AnalysisObject`
are
* [Counter](Counter.md)
* [Estimate](doc/Estimate.md)
* [BinnedDbn, BinnedHistoND and BinnedProfileND](doc/BinnedDbn.md)
* [BinnedEstimateND](doc/BinnedEstimate.md)
* [ScatterND](doc/Scatter.md)

# Dbn: the distribution class

YODA's main class to centralise the calculation of
statistics of unbounded unbinned samples distributions.
The `Dbn` class is templated on the number of dimensions:

```cpp
Dbn<N> d; // a N-dimensional distribution
```

Convenient aliases are provided for the first
few dimensions:

```cpp
Dbn0D d0;
Dbn1D d1;
Dbn2D d2;
Dbn3D d3;
```

Each distribution fill contributes a weight $w$.
Starting with 1D, dimensionful value terms
such as $\sum wx$ and $\sum wx^2$ are also
calculated.

By storing the total number of fills (ignoring weights),
$\sum w$, and $\sum w^2$, the `Dbn` class can calculate
the mean and error on the aggregate of the supplied weights.
It is used to provide this information in the
[Counter class](Counter.md) as well as
[histograms and profiles](BinnedDbn.md).

